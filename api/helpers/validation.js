const Joi = require('@hapi/joi');

const loginValidation = (userData) => {
  const userSchema = Joi.object({
    email: Joi.string().required().min(6).max(255).email(),
    password: Joi.string().required().min(6).max(1024),
});
  return userSchema.validate(userData);
}

const registerValidation = (userData) => {
  const userSchema = Joi.object({
    name: Joi.string().required().min(6),
    email: Joi.string().required().min(6).max(255).email(),
    password: Joi.string().required().min(6).max(1024),
  });
  return userSchema.validate(userData);
}

module.exports.loginValidation = loginValidation;
module.exports.registerValidation = registerValidation;
