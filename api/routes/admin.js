const r = require('express').Router();
const auth = require('../helpers/verifyJwt');

r.get('/', auth, (req, res) => {
  res.send('secret data');
});

module.exports = r;
