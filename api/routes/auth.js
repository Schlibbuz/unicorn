const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const r = require('express').Router();

const SALT_COMPLEXITY = 10;
const User = require('../models/user');

const { loginValidation, registerValidation } = require('../helpers/validation');

const validate = (f, data) => {
  return f(data);
};

r.post('/login', async (req, res) => {
  const validation = validate(loginValidation, req.body);

  if (validation.error) {
    return res.status(400).send(validation);
  }

  const user = await User.findOne({ email: req.body.email });
  if (
    !user
    ||
    !await bcrypt.compare(req.body.password, user.password)
  ) {
    return res.status(400).send("Auth failed");
  }

  res.header(
    'auth-token',
    jwt.sign({ _id: user._id }, process.env.UNICORN_SECRET)
  ).send("Logged in");
});

r.post('/register', async (req, res) => {

  const validation = validate(registerValidation, req.body);

  if (validation.error) {
    return res.status(400).send(validation);
  }

  const emailExists = await User.findOne({email: req.body.email});

  if (emailExists) {
    return res.status(400).send("Email already exists");
  }


  const user = new User({
    name: req.body.name,
    email: req.body.email,
    password: await bcrypt.hash(
      req.body.password,
      await bcrypt.genSalt(SALT_COMPLEXITY),
    ),
  });

  try {
    const savedUser = await user.save();
    res.send({
      status: `${savedUser.name} created`
    });
  } catch(err) {
    res.status(400).send(err);
  }
});

module.exports = r;
