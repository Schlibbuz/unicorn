require('dotenv').config();
const express = require('express');
const app = express();
const orm = require('mongoose');


app.use(express.json());

app.use('/admin', require('./routes/admin'));
app.use('/api/auth', require('./routes/auth'));

const dbConnString = `${process.env.DB_PROTO}://${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_SCHEMA}`;
orm.connect(
  dbConnString,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  },
  () => console.log(`orm connected to ${dbConnString}`)
);

app.listen(4220, () => console.log(`api listening on 0.0.0.0:${process.env.API_PORT}`));
